const { json } = require("body-parser");
const express = require("express");
const app = express();
const port = 3000;
app.use(json());
app.post("*", (req, res) => {
  console.log(`Request URL: ${req.url}`);
  console.log(`---Headers---`);
  Object.keys(req.headers).forEach((key) => {
    console.log(`${key}=${req.headers[key]}}`);
  });
  console.log("---Body---");
  console.log(req.body);
  console.log(`---End Request ${req.url}---`);
  res.send(`Response from gitlab-bot: URL ${req.url}`);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
